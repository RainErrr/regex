import React, {ReactElement} from 'react'

const App: React.FC = (): ReactElement => {
  return (
    <>Hello World</>
  )
}

export default App