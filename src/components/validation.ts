export const isValidEmail = (email: string) => {
  const regex = /^(\w+)@(\w+)\.(\w+)$/
  return regex.test(email)
}

export const isValidUrl = (url: string) => {
  const regex = /^https?/
  return regex.test(url)
}

export const startsWithAEndsWithZMultiline = (string: string) => {
  const regex = /^A(\w+)Z|\w$/m
  return regex.test(string)
}

export const isValidStringNoMatterTheCase = (string: string) => {
  const regex = /string/i
  return regex.test(string)
}

export const isValidCreditCardNumber = (creditCardNumber: string) => {
  const regex = /^[\d]{4}-[\d]{4}-[\d]{4}-[\d]{4}$/
  return regex.test(creditCardNumber)
}

export const doesNotContainABCSubstring = (string: string) => {
  const regex = /^((?!abc).)*$/
  return regex.test(string)
}

export const fetchDataFromTags = (html: string) => {
  const regex = /<p[^>]*>(.*?)<\/p>/g
  return html.match(regex)!
    .map(str => str.replace(/<\/?p[^>]*>/g, ''))
    .join(', ')
}
