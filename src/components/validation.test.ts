import {
  doesNotContainABCSubstring,
  fetchDataFromTags,
  isValidCreditCardNumber,
  isValidEmail,
  isValidStringNoMatterTheCase,
  isValidUrl,
  startsWithAEndsWithZMultiline,
} from './validation'

it('is valid email', () => {
  expect(isValidEmail('test@test.com')).toEqual(true)
  expect(isValidEmail('1est@test.co1')).toEqual(true)
})

it('is valid url', () => {
  expect(isValidUrl('https://www.test.com')).toEqual(true)
  expect(isValidUrl('http://www.test.com')).toEqual(true)
  expect(isValidUrl('1231323http://www.test.com')).toEqual(false)
})

it('checks if string starts with A and ends with Z while being multiline', () => {
  expect(startsWithAEndsWithZMultiline('Aasdasd\nasdasdasZ')).toEqual(true)
})

it('checks if the word string is valid no matter the input strings case', () => {
  expect(isValidStringNoMatterTheCase('StRiNg')).toEqual(true)
})

it('checks if the string is valid credit card number following pattern xxxx-xxxx-xxxx-xxxx', () => {
  expect(isValidCreditCardNumber('1111-2222-3333-4444')).toEqual(true)
  expect(isValidCreditCardNumber('0123-0456-0789-0000')).toEqual(true)
  expect(isValidCreditCardNumber('0123-0456-07891-0000')).toEqual(false)
  expect(isValidCreditCardNumber('0123-0456-0789-00001')).toEqual(false)
  expect(isValidCreditCardNumber('123-4567-0789-000')).toEqual(false)
})

it('checks that a string does not contain abc subStrings', () => {
  expect(doesNotContainABCSubstring('someabcsome')).toEqual(false)
  expect(doesNotContainABCSubstring('test')).toEqual(true)
})

it('gets data from html tags', () => {
  expect(fetchDataFromTags('<p>test</p>')).toEqual('test')
  expect(fetchDataFromTags('<p>test</p><p>data</p>')).toEqual('test, data')
  expect(fetchDataFromTags('<p>test</p> <p>data</p>')).toEqual('test, data')
  expect(fetchDataFromTags('<p class="test" id="testDataId">data</p><p id="testId">test</p>')).toEqual('data, test')
})