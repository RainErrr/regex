#Assigment

Implement different regex validations:
* Validate that string is email (contains alphanumeric + "@" + alphanumeric + "." + alphanumeric")
* Validate that string is `http/ https` url (starting with)
* Validate that string starts with `A`, ends with `Z`, considering it can be multiline
* Validate that matches word `string` when every letter can be both upper- or lower case
* Validate that string is credit card number `xxxx-xxxx-xxxx-xxx`
* Validate that string does not contain the substring "abc"
* There is a string which is html. Fetch everything inside `<p>` tags
    * There could be multiple `<p>` tags.
    * <p> tag can have attributes (class, id etc).

##Installation

* Make sure you have at least node 15 and relevant npm
* `git clone <project>` to clone project
* `npm i` for node modules

##Development:

###Run UI
`npm run dev`

###Test
`npm run test`

##Stack
* Typescript
* ReactJS
* Vite
* Bootstrap 5